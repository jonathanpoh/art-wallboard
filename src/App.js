import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  constructor() {
    super();

    this.state = {
      commits: []
    };
  }

  // repoUrl = () => ('https://api.bitbucket.org/2.0/repositories/ned/coverage/commits');

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to art-wallboard</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
      </div>
    );
  }

  componentDidMount() {
    fetch('https://api.bitbucket.org/2.0/repositories/ned/coveragepy/commits')
      .then(results => {
        return results.json();
      })
      .then(data => {
        console.log(data);
      });

  }
}

export default App;
